FROM node:16-alpine
WORKDIR /src/app
COPY . .
RUN npm install
EXPOSE 3000
CMD ["node", "index.js"]
