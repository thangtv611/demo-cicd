const express = require('express');
const morgan = require('morgan');

const app = express();

app.use(morgan('combined'));

app.get('/version', (req, res) => {
    return res.status(200).json({
        version: '0.0.1',
        status: 'running',
    });
});

app.get('/', (req, res) => {
    return res.status(200).json({
        hello: 'world',
    });
});

app.listen(3000, () => {
    console.log('listing on port 3000');
});
